import React from "react";
import { Header } from "../Components/Header";
import { ProductList } from "../Components/ProductList/ProductList";

export const ProductsPage: React.FC = () => {
  return (
    <>
      <Header />
      <ProductList />
    </>
  );
};
