import React, { useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

import { ProductsPage } from "./Pages/ProductsPage";

function App() {
  useEffect(() => {
    document.title = "Productenlijst";
  }, []);
  return <ProductsPage />;
}

export default App;
