import axios from "axios";

const API_URL = process.env.REACT_APP_API_BASE_URL;

export const API = axios.create({
  baseURL: API_URL,
});

export const APIStatus = {
  IDLE: "idle",
  LOADING: "loading",
  FAILED: " failed",
};
