import React, { Dispatch, SetStateAction } from "react";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { SortingAttributes } from "../../../Types/Types";

interface SortedByDropdownProps {
  sortedBy: SortingAttributes;
  onSortedByChange: Dispatch<SetStateAction<SortingAttributes>>;
}

export const SortedByDropdown: React.FC<SortedByDropdownProps> = (state) => {
  return (
    <DropdownButton size="sm" variant="secondary" title="Sorteer op">
      <Dropdown.Item
        onClick={() =>
          state.onSortedByChange({
            sortedBy: "id",
            sorted: 0,
            activeWhen: "idASC",
          })
        }
        active={"idASC" === state.sortedBy.activeWhen}
      >
        Populariteit
      </Dropdown.Item>
      <Dropdown.Item
        onClick={() =>
          state.onSortedByChange({
            sortedBy: "price",
            sorted: 0,
            activeWhen: "priceASC",
          })
        }
        active={"priceASC" === state.sortedBy.activeWhen}
      >
        Prijs laag - hoog
      </Dropdown.Item>
      <Dropdown.Item
        onClick={() =>
          state.onSortedByChange({
            sortedBy: "price",
            sorted: 1,
            activeWhen: "priceDESC",
          })
        }
        active={"priceDESC" === state.sortedBy.activeWhen}
      >
        Prijs hoog - laag
      </Dropdown.Item>
      <Dropdown.Item
        onClick={() =>
          state.onSortedByChange({
            sortedBy: "name",
            sorted: 0,
            activeWhen: "nameASC",
          })
        }
        active={"nameASC" === state.sortedBy.activeWhen}
      >
        Naam A - Z
      </Dropdown.Item>
      <Dropdown.Item
        onClick={() =>
          state.onSortedByChange({
            sortedBy: "name",
            sorted: 1,
            activeWhen: "nameDESC",
          })
        }
        active={"nameDESC" === state.sortedBy.activeWhen}
      >
        Naam Z - A
      </Dropdown.Item>
    </DropdownButton>
  );
};
