import React, { Dispatch, SetStateAction } from "react";
import { Dropdown, DropdownButton } from "react-bootstrap";

interface PerPageDropdownProps {
  productPerPage: number;
  onProductPerPageChange: Dispatch<SetStateAction<number>>;
}

export const PerPageDropdown: React.FC<PerPageDropdownProps> = (state) => {
  return (
    <DropdownButton size="sm" variant="info" title="Aantal per pagina">
      <Dropdown.Item
        onClick={() => state.onProductPerPageChange(10)}
        active={10 === state.productPerPage}
      >
        10
      </Dropdown.Item>
      <Dropdown.Item
        onClick={() => state.onProductPerPageChange(25)}
        active={25 === state.productPerPage}
      >
        25
      </Dropdown.Item>
      <Dropdown.Item
        onClick={() => state.onProductPerPageChange(50)}
        active={50 === state.productPerPage}
      >
        50
      </Dropdown.Item>
      <Dropdown.Item
        onClick={() => state.onProductPerPageChange(75)}
        active={75 === state.productPerPage}
      >
        75
      </Dropdown.Item>
      <Dropdown.Item
        onClick={() => state.onProductPerPageChange(100)}
        active={100 === state.productPerPage}
      >
        100
      </Dropdown.Item>
    </DropdownButton>
  );
};
