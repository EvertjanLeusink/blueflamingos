import React from "react";
import { Button, Col, Row } from "react-bootstrap";
import Image from "react-bootstrap/Image";
import { Product } from "../../Types/Types";

interface ProductCardProps {
  product: Product;
}

export const ProductCard: React.FC<ProductCardProps> = (product) => {
  return (
    <>
      <Col lg={2}>
        <Image src={product.product.product_image_link} thumbnail />
      </Col>
      <Col lg={8}>
        <Row>
          <strong style={{ fontSize: 15 }}>{product.product.name}</strong>
        </Row>
        <Row
          dangerouslySetInnerHTML={{ __html: product.product.description }}
        ></Row>
      </Col>
      <Col lg={2}>
        <Row>
          <strong>€{product.product.price}</strong>
        </Row>
        <Row className="mt-5">
          <Button href={product.product.partner_link} variant="success">
            Link naar site
          </Button>
        </Row>
      </Col>
    </>
  );
};
