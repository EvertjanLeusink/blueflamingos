import React, { useEffect, useState } from "react";
import {
  Alert,
  Col,
  Container,
  Pagination,
  Row,
  Spinner,
} from "react-bootstrap";
import {
  GetProductsResponse,
  Link,
  Product,
  SortingAttributes,
} from "../../Types/Types";
import { API, APIStatus } from "../../API/BackendAPI";
import { PerPageDropdown } from "./Dropdown/PerPageDropdown";
import { SortedByDropdown } from "./Dropdown/SortedByDropdown";
import { ProductCard } from "./ProductCard";

type APIStatusType = typeof APIStatus[keyof typeof APIStatus];

export const ProductList: React.FC = () => {
  const [status, setStatus] = useState<APIStatusType>(APIStatus.IDLE);
  const [productResponse, setProductResponse] = useState<GetProductsResponse>();
  const [productPerPage, setProductPerPage] = useState<number>(25);
  const [sortedBy, setSortedBy] = useState<SortingAttributes>({
    sortedBy: "id",
    sorted: 0,
    activeWhen: "idASC",
  });

  const getProducts = (url?: string) => {
    setStatus(APIStatus.LOADING);

    API.get(
      (url ? url : "/products?") +
        `&per_page=${productPerPage}&sort_by=${sortedBy.sortedBy}&DESC=${sortedBy.sorted}`
    )
      .then(function (response) {
        setProductResponse(response.data);
        setStatus(APIStatus.IDLE);
      })
      .catch((err) => setStatus(APIStatus.FAILED));
  };

  useEffect(() => {
    getProducts();
  }, [productPerPage, sortedBy]);

  return (
    <Container>
      <Col>
        <Row>
          <h1>Zoek een product</h1>
        </Row>
        <Row>
          <Col>
            <h5>
              {productPerPage} van de {productResponse?.total} resultaten
            </h5>
          </Col>
          <Col lg={2}>
            <PerPageDropdown
              productPerPage={productPerPage}
              onProductPerPageChange={setProductPerPage}
            />
          </Col>

          <Col>
            <SortedByDropdown
              sortedBy={sortedBy}
              onSortedByChange={setSortedBy}
            />
          </Col>
        </Row>
      </Col>
      <Col lg={12}>
        {status === APIStatus.LOADING ? (
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        ) : (
          productResponse?.data?.map((p: Product) => (
            <Row lg="4" className="mt-5">
              <ProductCard product={p} />
              <hr />
            </Row>
          ))
        )}
        {status === APIStatus.FAILED && (
          <Alert className="mt-5" variant="danger">
            Er is iets fout gegaan
          </Alert>
        )}
      </Col>
      <Pagination>
        {productResponse?.links?.map((l: Link) => (
          <Pagination.Item
            active={l.active}
            onClick={() => l.url && getProducts(l.url)}
          >
            <p dangerouslySetInnerHTML={{ __html: l.label }}></p>
          </Pagination.Item>
        ))}
      </Pagination>
    </Container>
  );
};
