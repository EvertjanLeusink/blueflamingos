import React from "react";
import { Navbar } from "react-bootstrap";

export const Header: React.FC = () => {
  return (
    <>
      <Navbar bg="dark">
        <Navbar.Brand href="#home">
          <img
            alt=""
            src="https://www.blueflamingos.nl/_next/image?url=%2Fimages%2Flogo.svg&w=256&q=75"
            width="100"
            height="70"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>
        <strong>Productenlijst</strong>
      </Navbar>
    </>
  );
};
