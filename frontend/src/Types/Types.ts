export interface SortingAttributes {
  sortedBy: string;
  sorted: number;
  activeWhen: String;
}

export interface Product {
  id: number;
  name: string;
  description: string;
  partner_link: string;
  product_image_link: string;
  price: number;
}

export interface Link {
  url?: string;
  label: string;
  active: boolean;
}

export interface GetProductsResponse {
  current_page: number;
  prev_page_url?: string;
  next_page_url?: string;
  total: number;
  links: Link[];
  data: Product[];
}
