<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $order = "ASC";
        if($request->input('DESC') == 1){
            $order = "DESC";
        }
        return Product::orderBy($request->input('sort_by', 'id'), $order)->paginate($request->input('per_page',10));
    }
}
