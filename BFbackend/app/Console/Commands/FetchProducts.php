<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
Use \Carbon\Carbon;
use App\Models\Product;
use Symfony\Component\Stopwatch\Stopwatch;


class FetchProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert all products into the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        $out = new \Symfony\Component\Console\Output\ConsoleOutput();
        $out->writeln("Fetching data");

        $stopwatch = new Stopwatch();
        $stopwatch->start("fetchdata");

        $data = Http::get('http://api.tradedoubler.com/1.0/productsUnlimited.json;fid=23056?token=26A8CEEC9833CED128CAC91910344740632FBC93');
        $insertData = [];

        $out->writeln("Making data");
        foreach($data->json()["products"] as $value){
            $data = [
                "name" =>  $value["name"],
                "description" => $value["description"],
                "partner_link" => $value["offers"][0]["productUrl"],
                "product_image_link" => $value["productImage"]["url"],
                "price" => (float)$value["offers"][0]["priceHistory"][0]["price"]["value"]
            ];
            $insertData[] = $data;
        }

        $out->writeln("Deleting old data");
        Product::query()->truncate();

        $out->writeln("Inserting data");
        $insertData = collect($insertData);
        $chunks = $insertData->chunk(1000);
        foreach($chunks as $chunk){
            \DB::table('products')->insert($chunk->toArray());
        }

        $out->writeln($stopwatch->stop('fetchdata'));
    }
}
