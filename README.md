Om het te runnen moet je nog even

Voor de frontend
 - in de directory npm install runnen, daarna frontend starten met npm start

Backend
- in de directory (BFbackend) composer update runnen
- de database migraten met php artisan migrate
- de API starten met php artisan serve
- en de producten kan je ophalen via het command php artisan fetch:products
